package com.court.generator;

import com.baomidou.mybatisplus.core.exceptions.MybatisPlusException;
import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.InjectionConfig;
import com.baomidou.mybatisplus.generator.config.*;
import com.baomidou.mybatisplus.generator.config.po.TableInfo;
import com.baomidou.mybatisplus.generator.config.rules.DateType;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;
import lombok.extern.slf4j.Slf4j;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

@Slf4j
public class AutoGenerato {
    private final static String projectPath = System.getProperty("user.dir") + "/generator";

    public static void main(String[] args) {
        // 代码生成器
        AutoGenerator mpg = new AutoGenerator();
        // 全局配置
        GlobalConfig gc = new GlobalConfig();
        gc.setOutputDir(projectPath + "/src/main/java");
        System.out.println("生成目录为:" + projectPath.replace("\\", "/") + "/src/main/java");
        //作者
        gc.setAuthor("Mybatis-plus");
        //是否打开输出目录
        gc.setOpen(false);
        //实体属性 Swagger2 注解
        gc.setSwagger2(true);
        //设置时间类型
        gc.setDateType(DateType.ONLY_DATE);
        //实体类后缀名
//        gc.setEntityName("%sDO");
        //覆盖已有文件
        gc.setFileOverride(true);
        //xml通用列
        gc.setBaseColumnList(false);
        //开启ar模式
        gc.setActiveRecord(true);
        //添加全局 相关配置
        mpg.setGlobalConfig(gc);

        // 数据源配置
        DataSourceConfig dsc = new DataSourceConfig();
        dsc.setDriverName("com.mysql.jdbc.Driver");

//        dsc.setUrl("jdbc:mysql://rm-wz970q2s0bg3p575n9o.mysql.rds.aliyuncs.com:3306/minor_art?useUnicode=true&characterEncoding=UTF-8&useSSL=false");
//        dsc.setUsername("zhongcai");
//        dsc.setPassword("HBFhbvgEP35jnycI");

        dsc.setUrl("jdbc:mysql://47.97.116.162:3306/test?useUnicode=true&characterEncoding=UTF-8&useSSL=false");
        dsc.setUsername("root");
        dsc.setPassword("javamaosuxing123456**");

//        dsc.setUrl("jdbc:mysql://rm-bp1815oldj0n0u91kwo.mysql.rds.aliyuncs.com/xinyi?useUnicode=true&characterEncoding=UTF-8&useSSL=false");
//        dsc.setUsername("xinyidb");
//        dsc.setPassword("Xinyipai0");

        mpg.setDataSource(dsc);

        // 包配置
        final PackageConfig pc = new PackageConfig();
//        pc.setModuleName(scanner("模块名"));
//        pc.setModuleName("test");
        pc.setParent("com.court.generator");
        //注入包 相关配置
        mpg.setPackageInfo(pc);

        // 自定义配置
        InjectionConfig cfg = new InjectionConfig() {
            @Override
            public void initMap() {
                // to do nothing
            }
        };

        // 如果模板引擎是 freemarker
        String templatePath = "/templates/mapper.xml.ftl";

        // 自定义输出配置
        List<FileOutConfig> focList = new ArrayList<FileOutConfig>();
        // 自定义配置会被优先输出
        focList.add(new FileOutConfig(templatePath) {
            @Override
            public String outputFile(TableInfo tableInfo) {
                // 自定义输出文件名 ， 如果你 Entity 设置了前后缀、此处注意 xml 的名称会跟着发生变化！！
                String xml_name = tableInfo.getEntityName();
                if (xml_name.endsWith("DO")) {
                    xml_name = tableInfo.getEntityName().substring(0, tableInfo.getEntityName().length() - 2);
                }
                return projectPath + "/src/main/java/com/court/generator/mapping/" + "/" + xml_name + "Mapper" + StringPool.DOT_XML;
            }
        });
        cfg.setFileOutConfigList(focList);
        mpg.setCfg(cfg);

        // 配置模板
        TemplateConfig templateConfig = new TemplateConfig();

        templateConfig.setXml(null);
        mpg.setTemplate(templateConfig);

        // 策略配置
        StrategyConfig strategy = new StrategyConfig();
        strategy.setNaming(NamingStrategy.underline_to_camel);
        strategy.setColumnNaming(NamingStrategy.underline_to_camel);
        strategy.setEntityBuilderModel(true);
        strategy.setEntityLombokModel(true);
        strategy.setRestControllerStyle(true);
        strategy.setInclude(scanner("表名，多个英文逗号分割").split(","));
        strategy.setTablePrefix(pc.getModuleName() + "_");
        //是否生成实体时，生成字段注解
        strategy.setEntityTableFieldAnnotationEnable(true);

        mpg.setStrategy(strategy);
        mpg.setTemplateEngine(new FreemarkerTemplateEngine());
        mpg.execute();
    }

    /**
     * <p>
     * 读取控制台内容
     * </p>
     */
    public static String scanner(String tip) {
        Scanner scanner = new Scanner(System.in);
        StringBuilder help = new StringBuilder();
        help.append("请输入" + tip + "：");
        System.out.println(help.toString());
        if (scanner.hasNext()) {
            String ipt = scanner.next();
            if (StringUtils.isNotEmpty(ipt)) {
                return ipt;
            }
        }
        throw new MybatisPlusException("请输入正确的" + tip + "！");
    }
}

