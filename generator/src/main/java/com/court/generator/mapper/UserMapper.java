package com.court.generator.mapper;

import com.court.generator.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author Mybatis-plus
 * @since 2019-11-30
 */
public interface UserMapper extends BaseMapper<User> {

}
