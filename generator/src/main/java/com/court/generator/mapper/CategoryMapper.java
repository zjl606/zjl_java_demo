package com.court.generator.mapper;

import com.court.generator.entity.Category;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Mybatis-plus
 * @since 2019-12-02
 */
public interface CategoryMapper extends BaseMapper<Category> {

}
