package com.court.generator.service;

import com.court.generator.entity.Category;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Mybatis-plus
 * @since 2019-12-02
 */
public interface ICategoryService extends IService<Category> {

}
