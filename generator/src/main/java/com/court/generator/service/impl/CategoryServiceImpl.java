package com.court.generator.service.impl;

import com.court.generator.entity.Category;
import com.court.generator.mapper.CategoryMapper;
import com.court.generator.service.ICategoryService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Mybatis-plus
 * @since 2019-12-02
 */
@Service
public class CategoryServiceImpl extends ServiceImpl<CategoryMapper, Category> implements ICategoryService {

}
