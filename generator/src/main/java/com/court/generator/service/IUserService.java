package com.court.generator.service;

import com.court.generator.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author Mybatis-plus
 * @since 2019-11-30
 */
public interface IUserService extends IService<User> {

}
