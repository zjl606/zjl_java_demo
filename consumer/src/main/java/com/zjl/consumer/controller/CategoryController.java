package com.zjl.consumer.controller;


import com.zjl.consumer.base.RetInfo;
import com.zjl.consumer.bean.user.Category;
import com.zjl.consumer.bean.user.SearchCategoryParam;
import com.zjl.consumer.bean.user.SearchUserParam;
import com.zjl.consumer.service.CategoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author Mybatis-plus
 * @since 2019-12-02
 */
@RestController
@Api(tags = "分类接口")
@RequestMapping("/category")
public class CategoryController {
    @Autowired
    CategoryService categoryService;

    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public RetInfo getList(@RequestBody SearchCategoryParam searchCategoryParam) {
        return categoryService.getList(searchCategoryParam);
    }

    @ApiOperation(value = "添加分类", notes = "添加分类")
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public RetInfo addCategory(@RequestBody Category category) {
        Date d = new Date();
        category.setCreateTime(d);
        category.setUpdateTime(d);
        return categoryService.createCategory(category);
    }


}
