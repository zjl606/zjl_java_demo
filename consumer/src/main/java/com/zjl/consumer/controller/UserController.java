package com.zjl.consumer.controller;

import com.zjl.consumer.base.RetInfo;
import com.zjl.consumer.bean.user.SearchUserParam;
import com.zjl.consumer.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Api(tags = "用户接口")
public class UserController {

    @Autowired
    UserService userService;

    @ApiOperation(value = "查询用户", notes = "查询用户")
    @RequestMapping(value = "/user/findUser",method = RequestMethod.POST)
    public RetInfo findUser(@RequestBody SearchUserParam searchUserParam) {
        return userService.findUser(searchUserParam);
    }

}
