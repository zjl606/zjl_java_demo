package com.zjl.consumer.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zjl.consumer.bean.user.User;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @since 2019-11-30
 */
@Repository
public interface UserMapper extends BaseMapper<User> {

}
