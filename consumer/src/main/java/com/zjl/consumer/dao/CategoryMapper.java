package com.zjl.consumer.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zjl.consumer.bean.user.Category;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RestController;
//import com.court.generator.entity.Category;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Mybatis-plus
 * @since 2019-12-02
 */
@Repository
public interface CategoryMapper extends BaseMapper<Category> {

}
