package com.zjl.consumer.bean.user;

import com.zjl.consumer.base.PageParam;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value="搜索用户请求对象", description="搜索用户请求对象")
public class SearchUserParam extends PageParam {

    /**
     * 用户姓名
     */
    @ApiModelProperty(name = "user_name",value = "用户姓名",notes = "可空",example = "刘")
    private String user_name;

}
