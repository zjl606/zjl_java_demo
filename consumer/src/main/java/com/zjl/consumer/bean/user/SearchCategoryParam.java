package com.zjl.consumer.bean.user;

import com.zjl.consumer.base.PageParam;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "搜索分类请求对象", description = "搜索分类请求对象")
public class SearchCategoryParam extends PageParam {
    @ApiModelProperty(name = "category_name", value = "分类",notes = "可空",example = "数码")
    private String category_name;
}
