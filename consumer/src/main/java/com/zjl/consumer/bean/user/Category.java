package com.zjl.consumer.bean.user;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 分类表
 * */

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="Category对象", description="分类表")

public class Category extends Model<Category> implements Serializable {
//    private static final long serialVersionUID = 1L;  // 序列化

    @ApiModelProperty(value = "分类主键id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer Id;

    @ApiModelProperty(value="分类名称")
    @TableField("category_name")
    private String categoryName;

    @ApiModelProperty(value = "分类上级ID")
    @TableField("parent_id")
    private Integer parentId;

    @ApiModelProperty(value="层级")
    @TableField("depth")
    private Integer depth;

    @ApiModelProperty(value="状态")
    @TableField("status")
    private Integer status;

    @ApiModelProperty(value = "优先级")
    @TableField("priority")
    private Integer priority;

    @ApiModelProperty(value = "描述")
    @TableField("desc")
    private String desc;

    @ApiModelProperty(value = "创建时间")
    @TableField("create_time")
    private Date createTime;

    @ApiModelProperty(value = "更新时间")
    @TableField("update_time")
    private Date updateTime;

    @Override
    protected Serializable pkVal() {
        return this.Id;
    }

}
