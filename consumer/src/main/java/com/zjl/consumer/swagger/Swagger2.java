package com.zjl.consumer.swagger;

import com.github.xiaoymin.swaggerbootstrapui.annotations.EnableSwaggerBootstrapUI;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.List;

@Configuration
@EnableSwagger2
@EnableSwaggerBootstrapUI
public class Swagger2 {

    @Bean
    public Docket createRestApi10() {
        return new Docket(DocumentationType.SWAGGER_2)
                .globalOperationParameters(parameterList())
                .apiInfo(apiInfo("测试服务", "测试服务", "1.0"))
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.zjl.consumer.controller"))
                .paths(PathSelectors.any())
                .build()
                .groupName("测试服务");
    }

    private ApiInfo apiInfo(String title, String description, String version) {
        Contact contact = new Contact("张景龙", "", "微信号:zjl405");
        return new ApiInfoBuilder()
                .title(title)//设置文档的标题
                .description(description)//设置文档的描述->1.Overview
                .contact(contact)//设置文档的联系方式
                .version(version)//设置文档的版本信息
                .build();
    }

    private List<Parameter> parameterList() {
        ParameterBuilder tokenPar = new ParameterBuilder();
        List<Parameter> pars = new ArrayList<>();
        tokenPar.name("token")
                .description("全局token参数,代表使用该接口需要登录")
//                .defaultValue("j9b6frb5lna9g0ubwesp")
                .required(false)
                .modelRef(new ModelRef("String"))
                .parameterType("header")
                .required(false).build();
        pars.add(tokenPar.build());
        return pars;
    }
}
