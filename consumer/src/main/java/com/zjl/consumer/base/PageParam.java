package com.zjl.consumer.base;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel(value="分页参数", description="分页参数")
public class PageParam implements Serializable {
    private static final long serialVersionUID = -7961111994469811986L;
    /**
     * 当前页
     */
    @ApiModelProperty(name = "page_num",value = "当前页",notes = "必传",example = "1",required = true)
    private Integer page_num;
    /**
     * 一页几条
     */
    @ApiModelProperty(name = "page_size",value = "一页几条",notes = "必传",example = "10",required = true)
    private Integer page_size;
}
