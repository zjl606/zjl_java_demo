package com.zjl.consumer.base;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;


@Data
@ApiModel(value="和前端交互的对象", description="和前端交互的对象")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RetInfo implements Serializable {

	private static final long serialVersionUID = 3727716032001744456L;

	@ApiModelProperty(value="状态,表示是否成功,0成功,非0失败", name="mark",example = "0")
	public String mark;
	@ApiModelProperty(value="提示语", name="tip",example = "成功")
	public String tip;
	@ApiModelProperty(value="返回的对象", name="obj")
	public Object obj;

	public RetInfo() {
	}

	public RetInfo(String mark, String tip) {
		this.mark = mark;
		this.tip = tip;
	}

	public RetInfo(String mark, String tip, Object obj) {
		this.mark = mark;
		this.tip = tip;
		this.obj = obj;
	}

}
