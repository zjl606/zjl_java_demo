package com.zjl.consumer.service;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zjl.consumer.base.RetInfo;
import com.zjl.consumer.bean.user.Category;
import com.zjl.consumer.bean.user.SearchCategoryParam;
import com.zjl.consumer.dao.CategoryMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@Service
public class CategoryService {
    @Autowired
    CategoryMapper categoryMapper;

    public RetInfo createCategory(Category category) {
        int insert = categoryMapper.insert(category);
        return new RetInfo("0", "sucees", insert);
    }

    public RetInfo updateCategory(Category category) {
        UpdateWrapper updateWrapper = new UpdateWrapper();  // 更新条件
        updateWrapper.set("category_name", category.getCategoryName());
        updateWrapper.eq("id", category.getId());
        int update = categoryMapper.update(category, updateWrapper);
        return new RetInfo("0", "success", update);
    }

    public RetInfo getList(SearchCategoryParam searchCategoryParam) {
        QueryWrapper queryWrapper = new QueryWrapper();
//        queryWrapper.eq("id", 1);
        queryWrapper.like(StrUtil.isNotEmpty(searchCategoryParam.getCategory_name()),"category_name", searchCategoryParam.getCategory_name());
        Page<Category> page = new Page<>(searchCategoryParam.getPage_num(), searchCategoryParam.getPage_size());
        IPage iPage = categoryMapper.selectPage(page, queryWrapper);

//        List list = categoryMapper.selectList(queryWrapper);
        Map map = new HashMap();
        if(iPage.getRecords().isEmpty()) {
            map.put("msg", "暂无数据");
            map.put("list", new ArrayList<Category>());
            return new RetInfo("0", "success", map);
        }
        map.put("msg", "获取分类列表");
        map.put("list", iPage);
        return new RetInfo("0", "success", map);
    }

    public RetInfo deleteCategory(Category category) {

        int deleteInfo = categoryMapper.deleteById(category.getId());
        return new RetInfo("0", "success", deleteInfo);
    }


}
