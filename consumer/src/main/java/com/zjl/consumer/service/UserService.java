package com.zjl.consumer.service;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zjl.consumer.base.RetInfo;
import com.zjl.consumer.bean.user.SearchUserParam;
import com.zjl.consumer.bean.user.User;
import com.zjl.consumer.dao.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Autowired
    UserMapper userMapper;

    /**
     * 查询用户
     */
    public RetInfo findUser(SearchUserParam searchUserParam) {
        //创建查询条件构造器
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.like(StrUtil.isNotEmpty(searchUserParam.getUser_name()), "user_name", searchUserParam.getUser_name());
        Page<User> page = new Page<>(searchUserParam.getPage_num(), searchUserParam.getPage_size());
        IPage iPage = userMapper.selectPage(page, queryWrapper);
        if (iPage.getRecords().isEmpty()) {
            return new RetInfo("1", "暂无数据");
        }
        return new RetInfo("0", "查询成功", iPage);
    }

}
